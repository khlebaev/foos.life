django.jQuery(document).on("formset:added", function(event, $row, formsetName) {
    var isRaceMatch = django.jQuery(".field-race_match .readonly")[0].innerText.toLowerCase() == "true";
    var gameTypeElement = $row.find("select[id$='-game_type']")[0];
    var orderElement = $row.find("input[id$='-order']")[0];
    var setElement = $row.find("input[id$='-set']")[0];

    if (isRaceMatch) {
        if (gameTypeElement) {
            SetGameTypeRaceMatch(gameTypeElement, $row[0].rowIndex)
        }
    }
    else {
        if (gameTypeElement) {
            SetGameType(gameTypeElement, $row[0].rowIndex)
        }

        if (setElement) {
            setElement.value = "";
        }
    }

    SetTeamScores(
        isRaceMatch,
        $row.find("input[id$='-team1_score']")[0],
        $row.find("input[id$='-team2_score']")[0],
        setElement)

    SetPlayers(
        $row.find("select[id$='-team1_player1']")[0],
        $row.find("select[id$='-team1_player2']")[0],
        $row.find("select[id$='-team2_player1']")[0],
        $row.find("select[id$='-team2_player2']")[0])

    if (orderElement) {
        orderElement.value = $row[0].rowIndex;
    }
});

django.jQuery(document).on("formset:removed", function(event, $row, formsetName) {
});

django.jQuery("tr[id^='game_set']").each(function() {
    SetTeamScores(
        django.jQuery(".field-race_match .readonly")[0].innerText.toLowerCase() == "true",
        django.jQuery(this).find("input[id$='-team1_score']")[0],
        django.jQuery(this).find("input[id$='-team2_score']")[0],
        django.jQuery(this).find("input[id$='-set']")[0])

    SetPlayers(
        django.jQuery(this).find("select[id$='-team1_player1']")[0],
        django.jQuery(this).find("select[id$='-team1_player2']")[0],
        django.jQuery(this).find("select[id$='-team2_player1']")[0],
        django.jQuery(this).find("select[id$='-team2_player2']")[0])
});

function SetTeamScores(isRaceMatch, team1ScoreElement, team2ScoreElement, setElement) {
    if (isRaceMatch) {
        if (team1ScoreElement) {
            team1ScoreElement.onchange = function() {
                var opponentScoreElement = this.parentNode.nextElementSibling.firstElementChild;
                var opponentsValue = AutoScoreOpponentValue(team1ScoreElement, opponentScoreElement, setElement, true);
                if (!isNaN(opponentsValue)) {
                    opponentScoreElement.value = opponentsValue;
                }
            }
        }

        if (team2ScoreElement) {
            team2ScoreElement.onchange = function() {
                var opponentScoreElement = this.parentNode.previousElementSibling.firstElementChild;
                var opponentsValue = AutoScoreOpponentValue(team2ScoreElement, opponentScoreElement, setElement, false);
                if (!isNaN(opponentsValue)) {
                    opponentScoreElement.value = opponentsValue;
                }
            }
        }
    }
    else {
        if (team1ScoreElement) {
            team1ScoreElement.onchange = function() {
                if (this.value && parseInt(this.value) != 5) {
                    this.parentNode.nextElementSibling.firstElementChild.value = 5;
                }
            }
        }

        if (team2ScoreElement) {
            team2ScoreElement.onchange = function() {
                if (this.value && parseInt(this.value) != 5) {
                    this.parentNode.previousElementSibling.firstElementChild.value = 5;
                }
            }
        }
    }
}

function SetPlayers(team1Player1Element, team1Player2Element, team2Player1Element, team2Player2Element) {
    if (team1Player1Element) {
        team1Player1Element.onchange = function(element) { SetPlayer(this, "select[id$='-team1_player1']"); }
    }

    if (team1Player2Element) {
        team1Player2Element.onchange = function(element) { SetPlayer(this, "select[id$='-team1_player2']"); }
    }

    if (team2Player1Element) {
        team2Player1Element.onchange = function(element) { SetPlayer(this, "select[id$='-team2_player1']"); }
    }

    if (team2Player2Element) {
        team2Player2Element.onchange = function(element) { SetPlayer(this, "select[id$='-team2_player2']"); }
    }
}

function SetPlayer(playerElement, playerIdSelector) {
    var gameType = django.jQuery(django.jQuery(playerElement).parents('tr')[0]).find("select[id$='-game_type']")[0];
    var setElement = django.jQuery(django.jQuery(playerElement).parents('tr')[0]).find("input[id$='-set']")[0];

    django.jQuery("tr[id^='game_set']").each(function() {
        var currentGameType = django.jQuery(this).find("select[id$='-game_type']")[0];
        var currentSetElement = django.jQuery(this).find("input[id$='-set']")[0];
        if (currentGameType && currentGameType.value == gameType.value && (!setElement.value || setElement.value == currentSetElement.value)) {
            django.jQuery(this).find(playerIdSelector)[0].value = playerElement.value;
        }
    });

    return;
}

function AutoScoreOpponentValue(scoreElement, opponentScoreElement, setElement, isTeam1) {
    if (scoreElement.value == "") {
        return NaN;
    }

    var gameScore = parseInt(scoreElement.value) || 0;
    var team1_total_set_score = 0;
    var team2_total_set_score = 0;

    django.jQuery("tr[id^='game_set']").each(function() {
        var currentSet = django.jQuery(this).find("input[id$='-set']")[0];
        if (currentSet && currentSet.value == setElement.value) {
            var team1_score_element = django.jQuery(this).find("input[id$='-team1_score']")[0];
            var team2_score_element = django.jQuery(this).find("input[id$='-team2_score']")[0];

            if (scoreElement != (isTeam1 ? team1_score_element : team2_score_element)) {
                team1_total_set_score += parseInt(team1_score_element.value) || 0;
            }
            if (opponentScoreElement != (isTeam1 ? team2_score_element : team1_score_element)) {
                team2_total_set_score += parseInt(team2_score_element.value) || 0;
            }
        }
    });

	var goals_remains = 5 - gameScore;
    var goals_remains_total = 40 - team1_total_set_score - team2_total_set_score;
	var set_score = isTeam1 ? team1_total_set_score : team2_total_set_score;
	var opponent_set_score = isTeam1 ? team2_total_set_score : team1_total_set_score;

    if ((goals_remains_total >= 0 && goals_remains_total <= 5) ||
		set_score + gameScore >= 20 ||
		opponent_set_score + goals_remains >= 20) {

        return NaN;
    }

    return goals_remains >= 0 && goals_remains <= 5
        ? goals_remains
        : NaN;
}

function SetGameTypeRaceMatch(gameTypeElement, gameNumber) {
    switch (gameNumber) {
        case 1:
        case 17:
        case 9:
            SetDropDownByText(gameTypeElement, "S1")
            break;

        case 2:
        case 10:
            SetDropDownByText(gameTypeElement, "D1")
            break;

        case 3:
        case 11:
            SetDropDownByText(gameTypeElement, "D2")
            break;

        case 4:
        case 12:
            SetDropDownByText(gameTypeElement, "S2")
            break;

        case 5:
        case 13:
            SetDropDownByText(gameTypeElement, "D1")
            break;

        case 6:
        case 14:
            SetDropDownByText(gameTypeElement, "S1")
            break;

        case 7:
        case 15:
            SetDropDownByText(gameTypeElement, "S2")
            break;

        case 8:
        case 16:
            SetDropDownByText(gameTypeElement, "D2")
            break;
    }
}

function SetGameType(gameTypeElement, gameNumber) {
    switch (gameNumber) {
        case 1:
        case 2:
            SetDropDownByText(gameTypeElement, "D1")
            break;

        case 3:
        case 4:
            SetDropDownByText(gameTypeElement, "D2")
            break;

        case 5:
        case 6:
            SetDropDownByText(gameTypeElement, "S1")
            break;

        case 7:
        case 8:
            SetDropDownByText(gameTypeElement, "S2")
            break;

        case 9:
        case 10:
            SetDropDownByText(gameTypeElement, "D3")
            break;

        case 11:
        case 12:
            SetDropDownByText(gameTypeElement, "D4")
            break;
    }
}

function SetDropDownByText(dropDown, textToFind) {
    for (var i = 0; i < dropDown.options.length; i++) {
        if (dropDown.options[i].text === textToFind) {
            dropDown.selectedIndex = i;
            break;
        }
    }
}