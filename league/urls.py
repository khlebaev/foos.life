from django.conf.urls import url

from . import views

app_name = 'league'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<city_short_name>\w{0,3})/$', views.index, name='index'),
    url(r'^(?P<city_short_name>\w{0,3})/players/$', views.player_statistics, name='player_statistics'),
    url(r'^(?P<city_short_name>\w{0,3})/matches/$', views.matches, name='matches'),
    url(r'^(?P<city_short_name>\w{0,3})/match/division/(?P<division_number>\d+)/(?P<round_number>\d+)/(?P<team1_short_name>\w{2,4})_(?P<team2_short_name>\w{2,4})/$',
        views.match_detail, name='match_detail'),
]