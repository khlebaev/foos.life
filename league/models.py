# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from itertools import groupby

from django.db import models
from django.db.models import Sum, Q
from django.db.models.signals import post_save


def places_upload_directory_path(instance, filename):
    return 'images/places/{0}/{1}'.format(instance.city.short_name, filename)


def teams_upload_directory_path(instance, filename):
    return 'images/teams/{0}/{1}/{2}/{3}'.format(instance.division.season.city.short_name, instance.division.season.number, instance.division.number, filename)


def teams_small_upload_directory_path(instance, filename):
    return 'images/teams/{0}/{1}/{2}/small/{3}'.format(instance.division.season.city.short_name, instance.division.season.number, instance.division.number, filename)


def player_sort(is_race_match):
    if is_race_match:
        return '-total_goal_average', \
               '-total_games_average', \
               '-total_singles_games_average', \
               '-total_scored_goals'
    else:
        return '-total_games_average', \
           '-total_singles_games_average', \
           '-total_goal_average', \
           '-total_scored_goals'


class City(models.Model):
    name = models.CharField(max_length=67)
    short_name = models.CharField(max_length=3)
    order = models.SmallIntegerField(default=0)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        db_table = 'city'
        ordering = ['order']


class Place(models.Model):
    city = models.ForeignKey(City)
    name = models.CharField(max_length=256)
    short_name = models.CharField(max_length=20, default='')
    image = models.ImageField(upload_to=places_upload_directory_path, blank=True)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        db_table = 'place'


class Season(models.Model):
    city = models.ForeignKey(City)
    name = models.CharField(max_length=45)
    number = models.DecimalField(max_digits=4, decimal_places=1)
    is_active = models.BooleanField()
    date_start = models.DateField()
    date_end = models.DateField()

    def number_for_url(self):
        return int(self.number) if self.number % 1 == 0 else self.number

    def __unicode__(self):
        return u'%s: %s' % (self.city, self.name)

    class Meta:
        db_table = 'season'
        ordering = ['city__order']


class Division(models.Model):
    season = models.ForeignKey(Season)
    name = models.CharField(max_length=45)
    number = models.PositiveSmallIntegerField()
    race_match_format = models.BooleanField(default=False)
    without_tie = models.BooleanField(default=False)

    @property
    def sorted_team_set(self):
        return self.team_set.order_by(
                '-total_match_points',
                '-total_set_points',
                '-total_goal_average'
            ) if self.race_match_format else \
                self.team_set.order_by(
                '-total_match_points',
                '-victories',
                '-total_game_points',
                '-total_goal_average'
            )

    @property
    def teams_matches(self):
        round_index = 1
        matches_by_round = []
        rounds = Match.objects.filter(team1__division_id=self.id).values('round_number').distinct().all()

        for round in rounds:
            matches = Match.objects.filter(team1__division_id=self.id).filter(round_number=round['round_number']).all()
            matches_by_team = []
            matches_by_team.append([{'display': ''}] + [{'display': x.short_name} for x in self.team_set.order_by('name').all()])
            is_race_match = self.race_match_format

            for index, team in enumerate(self.team_set.order_by('name').all()):
                matches_by_team_columns = [{'display': team.name}, ]

                for team_versus in self.team_set.order_by('name').all():
                    if team != team_versus:
                        match = next((x for x in matches if x.team1.id == team.id and x.team2.id == team_versus.id), None)
                        if match:
                            score = {'display': '%s/%s - %s/%s' % (match.team1_set1_score,
                                                                match.team2_set1_score,
                                                                match.team1_set2_score,
                                                                match.team2_set2_score) if is_race_match else
                                '%s - %s' % (match.team1_game_points, match.team2_game_points)}
                        else:
                            score = {'display': ''}
                        matches_by_team_columns.append(score)
                    else:
                        matches_by_team_columns.append({'is_blank': True})

                matches_by_team.append(matches_by_team_columns)
            matches_by_round.append({'name': '%s, %s круг' % (round_index, round_index + 1), 'table': matches_by_team})
            round_index += 2

        return matches_by_round

    @property
    def get_players(self):
        place_counter = 0
        for index, player in enumerate(TeamPlayer.objects \
            .filter(team__division_id=self.id) \
            .order_by(*player_sort(self.race_match_format))):
            place_counter += 0 if player.is_not_in_rating else 1
            yield place_counter if not player.is_not_in_rating else '', player

    def __unicode__(self):
        return u'%s %s' % (self.season, self.name)

    class Meta:
        db_table = 'division'


class Team(models.Model):
    division = models.ForeignKey(Division)
    captain = models.ForeignKey('TeamPlayer', related_name='captain_id', blank=True, null=True)
    name = models.CharField(max_length=45)
    short_name = models.CharField(max_length=4)
    image = models.ImageField(upload_to=teams_upload_directory_path, blank=True)
    image_small = models.ImageField(upload_to=teams_small_upload_directory_path, blank=True)
    played = models.PositiveSmallIntegerField(default=0, blank=True)
    victories = models.PositiveSmallIntegerField(default=0, blank=True)
    lost = models.PositiveSmallIntegerField(default=0, blank=True)
    tie = models.PositiveSmallIntegerField(default=0, blank=True)
    lost_by_penalty = models.PositiveSmallIntegerField(default=0, blank=True)
    total_match_points = models.PositiveSmallIntegerField(default=0, blank=True)
    total_set_points = models.PositiveSmallIntegerField(blank=True, null=True)
    total_game_points = models.SmallIntegerField(blank=True, null=True)
    total_goal_average = models.SmallIntegerField(default=0, blank=True)

    def get_matches(self):
        return Match.objects\
            .filter(Q(team1_id=self.id) | Q(team2_id=self.id))\
            .filter(Q(team1__division__id=self.division.id))\
            .order_by('-date')

    def lost_display(self):
        return self.lost - (self.lost_by_penalty if self.division.without_tie else 0)


    def __unicode__(self):
        return u'%s: %s' % (self.name, self.division)

    class Meta:
        db_table = 'team'


class Player(models.Model):
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    email = models.CharField(max_length=256, blank=True)

    def __unicode__(self):
        return u'%s %s' % (self.last_name, self.first_name)

    class Meta:
        db_table = 'player'


class TeamPlayer(models.Model):
    team = models.ForeignKey(Team)
    player = models.ForeignKey(Player)
    total_scored_goals = models.PositiveSmallIntegerField(default=0, blank=True)
    total_conceded_goals = models.PositiveSmallIntegerField(default=0, blank=True)
    total_goal_average = models.SmallIntegerField(default=0, blank=True)
    total_games_played = models.PositiveSmallIntegerField(default=0, blank=True)
    total_games_won = models.PositiveSmallIntegerField(default=0, blank=True)
    total_games_lost = models.PositiveSmallIntegerField(default=0, blank=True)
    total_games_tied = models.PositiveSmallIntegerField(default=0, blank=True)
    total_games_average = models.SmallIntegerField(default=0, blank=True)
    total_singles_games_played = models.PositiveSmallIntegerField(default=0, blank=True)
    total_singles_games_won = models.PositiveSmallIntegerField(default=0, blank=True)
    total_singles_games_lost = models.PositiveSmallIntegerField(default=0, blank=True)
    total_singles_games_tied = models.PositiveSmallIntegerField(default=0, blank=True)
    total_singles_games_average = models.SmallIntegerField(default=0, blank=True)
    total_doubles_games_played = models.PositiveSmallIntegerField(default=0, blank=True)
    total_doubles_games_won = models.PositiveSmallIntegerField(default=0, blank=True)
    total_doubles_games_lost = models.PositiveSmallIntegerField(default=0, blank=True)
    total_doubles_games_tied = models.PositiveSmallIntegerField(default=0, blank=True)
    total_doubles_games_average = models.SmallIntegerField(default=0, blank=True)
    is_not_in_rating = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s %s' % (self.player.last_name, self.player.first_name)

    class Meta:
        db_table = 'team_player'


class Match(models.Model):
    place = models.ForeignKey(Place)
    team1 = models.ForeignKey(Team, related_name='team1_id')
    team2 = models.ForeignKey(Team, related_name='team2_id')
    date = models.DateField()
    round_number = models.PositiveSmallIntegerField(default=1)
    team1_match_points = models.PositiveSmallIntegerField(default=0, blank=True)
    team2_match_points = models.PositiveSmallIntegerField(default=0, blank=True)
    team1_game_points = models.SmallIntegerField(blank=True, null=True)
    team2_game_points = models.SmallIntegerField(blank=True, null=True)
    team1_goals_scored = models.PositiveSmallIntegerField(blank=True, null=True)
    team1_goals_conceded = models.PositiveSmallIntegerField(blank=True, null=True)
    team2_goals_scored = models.PositiveSmallIntegerField(blank=True, null=True)
    team2_goals_conceded = models.PositiveSmallIntegerField(blank=True, null=True)
    team1_goal_average = models.SmallIntegerField(blank=True, null=True)
    team2_goal_average = models.SmallIntegerField(blank=True, null=True)
    team1_set1_score = models.PositiveSmallIntegerField(blank=True, null=True)
    team2_set1_score = models.PositiveSmallIntegerField(blank=True, null=True)
    team1_set2_score = models.PositiveSmallIntegerField(blank=True, null=True)
    team2_set2_score = models.PositiveSmallIntegerField(blank=True, null=True)
    team1_set1_points = models.PositiveSmallIntegerField(blank=True, null=True)
    team2_set1_points = models.PositiveSmallIntegerField(blank=True, null=True)
    team1_set2_points = models.PositiveSmallIntegerField(blank=True, null=True)
    team2_set2_points = models.PositiveSmallIntegerField(blank=True, null=True)
    team1_set_points = models.PositiveSmallIntegerField(blank=True, null=True)
    team2_set_points = models.PositiveSmallIntegerField(blank=True, null=True)
    is_team1_forfeit = models.BooleanField(default=False)
    is_team2_forfeit = models.BooleanField(default=False)

    def is_team1(self, team_id):
        return self.team1.id == team_id

    def team_match_points(self, team_id):
        return self.team1_match_points if self.is_team1(team_id) else self.team2_match_points

    def team_set_points(self, team_id):
        set_points = self.team1_set_points if self.is_team1(team_id) else self.team2_set_points
        return set_points if set_points else 0

    def team_game_average(self, team_id):
        game_points = self.team1_game_points if self.is_team1(team_id) else self.team2_game_points
        opponent_game_points = self.team2_game_points if self.is_team1(team_id) else self.team1_game_points
        game_points_average = (game_points if game_points else 0) - (opponent_game_points if opponent_game_points else 0)
        return game_points_average

    def team_goal_average(self, team_id):
        return self.team1_goal_average if self.is_team1(team_id) else self.team2_goal_average

    def is_team_won(self, team_id):
        return self.team1_match_points > self.team2_match_points if self.is_team1(team_id) \
            else self.team2_match_points > self.team1_match_points

    def is_teams_tied(self):
        return self.team1_match_points == self.team2_match_points

    def team_lost_by_penalty(self, team_id):
        team1_penalty_score = self.penalty_set.filter(is_team1_scored=True).count()
        team2_penalty_score = self.penalty_set.filter(is_team2_scored=True).count()

        return team1_penalty_score < team2_penalty_score if self.is_team1(team_id) \
            else team1_penalty_score > team2_penalty_score

    def penalty_score(self):
        team1_penalty_score = self.penalty_set.filter(is_team1_scored=True).count()
        team2_penalty_score = self.penalty_set.filter(is_team2_scored=True).count()
        return u'%s : %s' % (team1_penalty_score, team2_penalty_score ) if team1_penalty_score != 0 or team2_penalty_score != 0 else ''

    def team1_set_score(self):
        return (1 if self.team1_set1_score > self.team2_set1_score else 0) +\
               (1 if self.team1_set2_score > self.team2_set2_score else 0)

    def team2_set_score(self):
        return (1 if self.team2_set1_score > self.team1_set1_score else 0) +\
               (1 if self.team2_set2_score > self.team1_set2_score else 0)

    def sets_score_display(self):
        return u'%s:%s, %s:%s' % (self.team1_set1_score, self.team2_set1_score, self.team1_set2_score, self.team2_set2_score)

    def games(self):
        result = []
        games = sorted(self.game_set.all(), key=lambda x: x.order)

        for game_type, games_grouped_by_type in groupby(games, lambda x: x.game_type_id):
            team1_score = []
            team2_score = []
            team1_player1 = ''
            team1_player2 = ''
            team2_player1 = ''
            team2_player2 = ''

            for game in games_grouped_by_type:
                team1_player1 = game.team1_player1
                team1_player2 = game.team1_player2
                team2_player1 = game.team2_player1
                team2_player2 = game.team2_player2
                team1_score.append({
                    'is_win': game.team1_score > game.team2_score,
                    'balls': game.team1_score
                })
                team2_score.append({
                    'is_win': game.team2_score > game.team1_score,
                    'balls': game.team2_score
                })

            game_info = {
                'game_type': GameType.objects.get(id=game_type).name,
                'team1': {
                    'player1': team1_player1,
                    'player2': team1_player2,
                    'score': team1_score
                },
                'team2': {
                    'player1': team2_player1,
                    'player2': team2_player2,
                    'score': team2_score
                }
            }

            result.append(game_info)

        return result

    def games_by_set(self, set_number):
        result = []
        games = sorted(self.game_set.filter(set=set_number).all(), key=lambda x: x.order)

        for game in games:
            team1_player1 = game.team1_player1
            team1_player2 = game.team1_player2
            team2_player1 = game.team2_player1
            team2_player2 = game.team2_player2
            team1_score = {
                'is_win': game.team1_score > game.team2_score,
                'balls': game.team1_score
            }
            team2_score = {
                'is_win': game.team2_score > game.team1_score,
                'balls': game.team2_score
            }
            game_info = {
                'game_type': game.game_type.name,
                'team1': {
                    'player1': team1_player1,
                    'player2': team1_player2,
                    'score': team1_score
                },
                'team2': {
                    'player1': team2_player1,
                    'player2': team2_player2,
                    'score': team2_score
                }
            }
            result.append(game_info)

        return result

    def __unicode__(self):
        return u'%s vs %s %s %s' % (self.team1.short_name, self.team2.short_name, self.date, self.place)

    class Meta:
        db_table = 'match'

    def save(self, *args, **kwargs):
        super(Match, self).save(*args, **kwargs)

        is_race_match = self.team1.division.race_match_format
        team1_goals_scored = 0 if self.is_team1_forfeit or self.is_team2_forfeit else self.game_set.aggregate(goals_scored=Sum('team1_score'))['goals_scored']
        team2_goals_scored = 0 if self.is_team1_forfeit or self.is_team2_forfeit else self.game_set.aggregate(goals_scored=Sum('team2_score'))['goals_scored']

        self.team1_goals_scored = team1_goals_scored
        self.team1_goals_conceded = team2_goals_scored
        self.team2_goals_scored = team2_goals_scored
        self.team2_goals_conceded = team1_goals_scored

        if team1_goals_scored is not None and team2_goals_scored is not None:
            self.team1_goal_average = team1_goals_scored - team2_goals_scored
            self.team2_goal_average = team2_goals_scored - team1_goals_scored

        if is_race_match:
            if self.is_team1_forfeit or self.is_team2_forfeit:
                self.team1_set1_score = team1_set1_score = 7 if self.is_team1_forfeit else 21 if self.is_team2_forfeit else 0
                self.team1_set2_score = team1_set2_score = 7 if self.is_team1_forfeit else 21 if self.is_team2_forfeit else 0
                self.team2_set1_score = team2_set1_score = 21 if self.is_team1_forfeit else 7 if self.is_team2_forfeit else 0
                self.team2_set2_score = team2_set2_score = 21 if self.is_team1_forfeit else 7 if self.is_team2_forfeit else 0
            else:
                team1_set1_score = self.game_set.filter(set=1).aggregate(set_sum=Sum('team1_score'))['set_sum']
                team2_set1_score = self.game_set.filter(set=1).aggregate(set_sum=Sum('team2_score'))['set_sum']
                team1_set2_score = self.game_set.filter(set=2).aggregate(set_sum=Sum('team1_score'))['set_sum']
                team2_set2_score = self.game_set.filter(set=2).aggregate(set_sum=Sum('team2_score'))['set_sum']

            is_team1_set1_win = team1_set1_score == 21
            is_team1_set2_win = team1_set2_score == 21
            is_team2_set1_win = team2_set1_score == 21
            is_team2_set2_win = team2_set2_score == 21
            is_set1_tie = team1_set1_score == team2_set1_score == 20
            is_set2_tie = team1_set2_score == team2_set2_score == 20
            is_team1_set1_lost = team2_set1_score == 21
            is_team1_set2_lost = team2_set2_score == 21

            team1_set1_points = 2 if is_team1_set1_win else 1 if is_set1_tie else 0
            team1_set2_points = 2 if is_team1_set2_win else 1 if is_set2_tie else 0
            team2_set1_points = 2 if is_team2_set1_win else 1 if is_set1_tie else 0
            team2_set2_points = 2 if is_team2_set2_win else 1 if is_set2_tie else 0

            self.team1_set1_score = team1_set1_score
            self.team2_set1_score = team2_set1_score
            self.team1_set2_score = team1_set2_score
            self.team2_set2_score = team2_set2_score
            self.team1_set1_points = team1_set1_points
            self.team2_set1_points = team2_set1_points
            self.team1_set2_points = team1_set2_points
            self.team2_set2_points = team2_set2_points
            self.team1_set_points = team1_set1_points + team1_set2_points
            self.team2_set_points = team2_set1_points + team2_set2_points

            is_team1_win = (is_team1_set1_win and is_team1_set2_win) or \
                           (is_set1_tie and is_team1_set2_win) or \
                           (is_set2_tie and is_team1_set1_win)

            is_team2_win = (is_team2_set1_win and is_team2_set2_win) or \
                           (is_set1_tie and is_team2_set2_win) or \
                           (is_set2_tie and is_team2_set1_win)

            is_tie = (is_set1_tie and is_set2_tie) or \
                     (is_team1_set1_win and is_team1_set2_lost) or \
                     (is_team1_set1_lost and is_team1_set2_win)

            self.team1_match_points = 2 if is_team1_win else 1 if is_tie else 0
            self.team2_match_points = 2 if is_team2_win else 1 if is_tie else 0

        else:
            team1_game_points = 0
            team2_game_points = 0

            if self.is_team1_forfeit or self.is_team2_forfeit:
                team1_game_points = 0 if self.is_team1_forfeit and self.is_team2_forfeit else 0 if self.is_team1_forfeit else 4
                team2_game_points = 0 if self.is_team1_forfeit and self.is_team2_forfeit else 0 if self.is_team2_forfeit else 4
            else:
                games = sorted(self.game_set.all(), key=lambda x: x.game_type_id)
                for game_type, games_grouped_by_type in groupby(games, lambda x: x.game_type_id):
                    team1_win_set_count = 0

                    for game in games_grouped_by_type:
                        is_team1_set_win = game.team1_score > game.team2_score
                        team1_win_set_count += 1 if is_team1_set_win else 0

                    if team1_win_set_count == 2:
                        team1_game_points += 1
                    elif team1_win_set_count == 0:
                        team2_game_points += 1

            self.team1_game_points = team1_game_points
            self.team2_game_points = team2_game_points

            is_team1_win = team1_game_points > team2_game_points
            is_tie = team1_game_points == team2_game_points
            is_team2_win = team2_game_points > team1_game_points

            if is_tie:
                self.team1_match_points = 1 if self.team_lost_by_penalty(self.team1.id) else 2 if not (self.is_team1_forfeit and self.is_team2_forfeit) else 0
                self.team2_match_points = 1 if self.team_lost_by_penalty(self.team2.id) else 2 if not (self.is_team1_forfeit and self.is_team2_forfeit) else 0
            else:
                self.team1_match_points = 2 if is_team1_win else 0
                self.team2_match_points = 2 if is_team2_win else 0

        super(Match, self).save(*args, **kwargs)


class Game(models.Model):
    match = models.ForeignKey(Match)
    game_type = models.ForeignKey('GameType')
    team1_player1 = models.ForeignKey(TeamPlayer, related_name='team1_player1_id')
    team1_player2 = models.ForeignKey(TeamPlayer, related_name='team1_player2_id', blank=True, null=True)
    team2_player1 = models.ForeignKey(TeamPlayer, related_name='team2_player1_id')
    team2_player2 = models.ForeignKey(TeamPlayer, related_name='team2_player2_id', blank=True, null=True)
    team1_score = models.PositiveSmallIntegerField()
    team2_score = models.PositiveSmallIntegerField()
    is_forfeit = models.BooleanField(default=False, blank=True)
    set = models.PositiveSmallIntegerField(default=1, blank=True, null=True)
    order = models.SmallIntegerField(default=0)

    def is_team1_player(self, player_id):
        return (self.team1_player1 is not None and self.team1_player1.id == player_id) or \
               (self.team1_player2 is not None and self.team1_player2.id == player_id)

    def player_scored_goals(self, player_id):
        return self.team1_score if self.is_team1_player(player_id) else self.team2_score

    def player_conceded_goals(self, player_id):
        return self.team2_score if self.is_team1_player(player_id) else self.team1_score

    class Meta:
        db_table = 'game'


class GameType(models.Model):
    name = models.CharField(max_length=3)
    is_single = models.BooleanField(default=False)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        db_table = 'game_type'


class Penalty(models.Model):
    match = models.ForeignKey(Match)
    team1_player = models.ForeignKey(TeamPlayer, related_name='team1_player', blank=True, null=True)
    team2_player = models.ForeignKey(TeamPlayer, related_name='team2_player', blank=True, null=True)
    is_team1_scored = models.BooleanField()
    is_team2_scored = models.BooleanField()
    order = models.SmallIntegerField(default=0)

    class Meta:
        db_table = 'penalty'
