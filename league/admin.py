# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from itertools import groupby

from django.contrib import admin
from django.db.models import Q

from .admin_utils.mixins import LimitedAdminInlineMixin
from .models import City, Place, Season, Division, Team, Player, TeamPlayer, Match, Game, GameType, Penalty


class TeamPlayerInline(admin.TabularInline):
    model = TeamPlayer
    fields = ('player',)


class TeamAdmin(admin.ModelAdmin):
    actions = ['calculate_team']
    inlines = [
        TeamPlayerInline,
    ]

    def calculate_team(modeladmin, request, queryset):
        for team in queryset:
            team.played = 0
            team.victories = 0
            team.lost = 0
            team.tie = 0
            team.lost_by_penalty = 0
            team.total_match_points = 0
            team.total_set_points = 0
            team.total_game_points = 0
            team.total_goal_average = 0
            team.save()

            for match in Match.objects.filter(Q(team1_id=team.id) | Q(team2_id=team.id)).order_by('date'):
                is_team_won = match.is_team_won(team.id)
                team.played += 1
                team.victories += 1 if is_team_won else 0
                team.lost += 1 if not is_team_won and not match.is_teams_tied() else 0
                team.tie += 1 if match.is_teams_tied() else 0
                team.lost_by_penalty += 1 if match.team_lost_by_penalty(team.id) else 0
                team.total_match_points += match.team_match_points(team.id)
                team.total_set_points += match.team_set_points(team.id)
                team.total_game_points += match.team_game_average(team.id)
                team.total_goal_average += match.team_goal_average(team.id)
                team.save()


class GameAdmin(admin.ModelAdmin):
    model = Game
    list_display = ('id', 'team1_player1', 'team1_player2', 'team1_score', 'team2_score', 'team2_player1',
                    'team2_player2', 'order',)
    list_editable = ('order',)


class GameInline(LimitedAdminInlineMixin, admin.TabularInline):
    model = Game
    ordering = ('order', 'id',)

    def get_extra(self, request, obj=None, **kwargs):
        return 0

    def get_filters(self, obj):
        return (('team1_player1', {'team__id': obj.team1_id if obj else None}),
                ('team1_player2', {'team__id': obj.team1_id if obj else None}),
                ('team2_player1', {'team__id': obj.team2_id if obj else None}),
                ('team2_player2', {'team__id': obj.team2_id if obj else None}),)


class PenaltyInline(LimitedAdminInlineMixin, admin.TabularInline):
    model = Penalty
    ordering = ('order', 'id',)

    def get_extra(self, request, obj=None, **kwargs):
        return 0

    def get_filters(self, obj):
        return (('team1_player', {'team__id': obj.team1_id if obj else None}),
                ('team2_player', {'team__id': obj.team2_id if obj else None}),)


def match_teams_display(obj):
    return '%s: %s vs. %s, %s' % (obj.date, obj.team1.name, obj.team2.name, obj.place)


def match_points_display(obj):
    return '%s:%s' % (obj.team1_match_points, obj.team2_match_points)


def match_set_points_display(obj):
    return '%s:%s' % (obj.team1_set_points, obj.team2_set_points)


def match_game_points_display(obj):
    return '%s:%s' % (obj.team1_game_points, obj.team2_game_points)


def match_goal_average_display(obj):
    return '%s:%s' % (obj.team1_goal_average, obj.team2_goal_average)


def race_match(obj):
    return obj.team1.division.race_match_format


class MatchAdmin(admin.ModelAdmin):
    list_display = (match_teams_display, match_points_display, match_set_points_display, match_game_points_display,
                    match_goal_average_display,)
    readonly_fields = (race_match,)
    inlines = [
        GameInline,
        PenaltyInline,
    ]
    ordering = ('-date',)

    def get_formsets_with_inlines(self, request, obj=None):
        for inline in self.get_inline_instances(request, obj):
            # hide MyInline in the add view
            if isinstance(inline, GameInline) and obj is None:
                continue
            yield inline.get_formset(request, obj), inline


class TeamPlayerAdmin(admin.ModelAdmin):
    model = TeamPlayer
    list_display = ('player', 'team',)
    actions = ['calculate_player']

    def calculate_player(modeladmin, request, queryset):
        for player in queryset:
            player.total_scored_goals = 0
            player.total_conceded_goals = 0
            player.total_goal_average = 0
            player.total_games_played = 0
            player.total_games_won = 0
            player.total_games_lost = 0
            player.total_games_tied = 0
            player.total_games_average = 0
            player.total_singles_games_played = 0
            player.total_singles_games_won = 0
            player.total_singles_games_lost = 0
            player.total_singles_games_tied = 0
            player.total_singles_games_average = 0
            player.total_doubles_games_played = 0
            player.total_doubles_games_won = 0
            player.total_doubles_games_lost = 0
            player.total_doubles_games_tied = 0
            player.total_doubles_games_average = 0
            player.save()

            for match in Match.objects\
                    .filter(Q(game__team1_player1_id=player.id) | Q(game__team1_player2_id=player.id) |
                            Q(game__team2_player1_id=player.id) | Q(game__team2_player2_id=player.id))\
                    .distinct():

                is_team1 = match.team1.teamplayer_set.filter(id=player.id).count() > 0

                if match.team1.division.race_match_format:
                    for game in match.game_set.filter(Q(team1_player1=player.id) |
                                                    Q(team1_player2=player.id) |
                                                    Q(team2_player1=player.id) |
                                                    Q(team2_player2=player.id))\
                                            .filter(is_forfeit=False)\
                                            .order_by('match__date', 'order'):
                        is_win = game.team1_score > game.team2_score if is_team1 else game.team2_score > game.team1_score
                        is_lost = not is_win
                        is_single = game.game_type.is_single
                        is_double = not is_single
                        player.total_scored_goals += game.player_scored_goals(player.id)
                        player.total_conceded_goals += game.player_conceded_goals(player.id)
                        player.total_games_played += 1
                        player.total_games_won += 1 if is_win else 0
                        player.total_games_lost += 1 if is_lost else 0
                        player.total_singles_games_played += 1 if is_single else 0
                        player.total_singles_games_won += 1 if is_win and is_single else 0
                        player.total_singles_games_lost += 1 if is_lost and is_single else 0
                        player.total_doubles_games_played += 1 if is_double else 0
                        player.total_doubles_games_won += 1 if is_win and is_double else 0
                        player.total_doubles_games_lost += 1 if is_lost and is_double else 0
                        player.save()
                else:
                    games = sorted(
                        match.game_set.filter(Q(team1_player1_id=player.id) | Q(team1_player2_id=player.id)).filter(is_forfeit=False).all() \
                        if is_team1 \
                        else match.game_set.filter(Q(team2_player1_id=player.id) | Q(team2_player2_id=player.id)).filter(is_forfeit=False).all(),
                        key=lambda x: x.game_type_id)

                    total_scored_goals = 0
                    total_conceded_goals = 0
                    games_played = 0
                    games_win = 0
                    games_lost = 0
                    games_tied = 0
                    singles_games_played = 0
                    singles_games_win = 0
                    singles_games_lost = 0
                    singles_games_tied = 0
                    doubles_games_played = 0
                    doubles_games_win = 0
                    doubles_games_lost = 0
                    doubles_games_tied = 0

                    for game_type_id, games_grouped_by_type in groupby(games, lambda x: x.game_type_id):
                        games_played += 1
                        win_set_count = 0

                        for game in games_grouped_by_type:
                            total_scored_goals += game.player_scored_goals(player.id)
                            total_conceded_goals += game.player_conceded_goals(player.id)
                            is_set_win = game.team1_score > game.team2_score if is_team1 else game.team2_score > game.team1_score
                            win_set_count += 1 if is_set_win else 0

                        is_win = win_set_count == 2
                        is_lost = win_set_count == 0
                        is_tied = win_set_count == 1
                        is_single = GameType.objects.get(id=game_type_id).is_single
                        is_double = not is_single

                        games_win += 1 if is_win else 0
                        games_lost += 1 if is_lost else 0
                        games_tied += 1 if is_tied else 0
                        singles_games_played += 1 if is_single else 0
                        singles_games_win += 1 if is_win and is_single else 0
                        singles_games_lost += 1 if is_lost and is_single else 0
                        singles_games_tied += 1 if is_tied and is_single else 0
                        doubles_games_played += 1 if is_double else 0
                        doubles_games_win += 1 if is_win and is_double else 0
                        doubles_games_lost += 1 if is_lost and is_double else 0
                        doubles_games_tied += 1 if is_tied and is_double else 0

                    player.total_scored_goals += total_scored_goals
                    player.total_conceded_goals += total_conceded_goals
                    player.total_games_played += games_played
                    player.total_games_won += games_win
                    player.total_games_lost += games_lost
                    player.total_games_tied += games_tied
                    player.total_singles_games_played += singles_games_played
                    player.total_singles_games_won += singles_games_win
                    player.total_singles_games_lost += singles_games_lost
                    player.total_singles_games_tied += singles_games_tied
                    player.total_doubles_games_played += doubles_games_played
                    player.total_doubles_games_won += doubles_games_win
                    player.total_doubles_games_lost += doubles_games_lost
                    player.total_doubles_games_tied += doubles_games_tied
                    player.save()

            player.total_goal_average = player.total_scored_goals - player.total_conceded_goals
            player.total_games_average = player.total_games_won - player.total_games_lost
            player.total_singles_games_average = player.total_singles_games_won - player.total_singles_games_lost
            player.total_doubles_games_average = player.total_doubles_games_won - player.total_doubles_games_lost
            player.save()


admin.site.register(City)
admin.site.register(Place)
admin.site.register(Season)
admin.site.register(Division)
admin.site.register(Team, TeamAdmin)
admin.site.register(Player)
admin.site.register(TeamPlayer, TeamPlayerAdmin)
admin.site.register(Match, MatchAdmin)
admin.site.register(Game, GameAdmin)
admin.site.register(GameType)
