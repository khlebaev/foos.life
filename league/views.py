# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db.models import Q
from django.http import Http404
from django.shortcuts import get_object_or_404, get_list_or_404, render
from .models import Season, Match, Team


def index(request, city_short_name=''):
    active_season = get_list_or_404(Season,
                                     Q(city__short_name__iexact=city_short_name) if city_short_name else Q(),
                                     is_active=True)[0]

    return render(request,
                  'start_page/index.html',
                  {
                      'active_season': active_season,
                      'selected_city': city_short_name if city_short_name else active_season.city.short_name
                   })


def player_statistics(request, city_short_name):
    active_season = get_object_or_404(Season,
                                     Q(city__short_name__iexact=city_short_name) if city_short_name else Q(),
                                     is_active=True)

    return render(request,
                  'player_statistics/list.html',
                  {
                      'active_season': active_season,
                      'selected_city': city_short_name if city_short_name else active_season.city.short_name
                  })


def matches(request, city_short_name):
    active_season = get_object_or_404(Season,
                                      Q(city__short_name__iexact=city_short_name) if city_short_name else Q(),
                                      is_active=True)

    return render(request,
                  'match/list.html',
                  {
                      'active_season': active_season,
                      'selected_city': city_short_name if city_short_name else active_season.city.short_name
                  })

def match_detail(request, city_short_name, division_number, round_number, team1_short_name, team2_short_name):
    active_season = get_object_or_404(Season,
                                      Q(city__short_name__iexact=city_short_name) if city_short_name else Q(),
                                      is_active=True)
    match = get_object_or_404(Match,
                              team1__division__season__city__short_name__iexact=city_short_name,
                              team1__division__season__number=active_season.number,
                              team1__division__number=division_number,
                              round_number=round_number,
                              team1__short_name__iexact=team1_short_name,
                              team2__short_name__iexact=team2_short_name)

    if match.is_team1_forfeit or match.is_team2_forfeit:
        raise Http404()

    is_race_match = match.team1.division.race_match_format

    games = match.game_set.all()

    team1_set1_scores = {}
    team2_set1_scores = {}
    team1_set2_scores = {}
    team2_set2_scores = {}
    team_1_set1_sum = 0
    team_2_set1_sum = 0
    team_1_set2_sum = 0
    team_2_set2_sum = 0

    if is_race_match:
        for set1_games in games.filter(set=1).order_by('order'):
            team_1_set1_sum += set1_games.team1_score
            team_2_set1_sum += set1_games.team2_score

            team1_set1_scores[team_1_set1_sum] =\
                (team1_set1_scores[team_1_set1_sum] + ', ' if team1_set1_scores.has_key(team_1_set1_sum) else '') \
                + set1_games.game_type.name

            team2_set1_scores[team_2_set1_sum] = \
                (team2_set1_scores[team_2_set1_sum] + ', ' if team2_set1_scores.has_key(team_2_set1_sum) else '') \
                + set1_games.game_type.name

        for set2_games in games.filter(set=2).order_by('order'):
            team_1_set2_sum += set2_games.team1_score
            team_2_set2_sum += set2_games.team2_score

            team1_set2_scores[team_1_set2_sum] =\
                (team1_set2_scores[team_1_set2_sum] + ', ' if team1_set2_scores.has_key(team_1_set2_sum) else '') \
                + set2_games.game_type.name

            team2_set2_scores[team_2_set2_sum] = \
                (team2_set2_scores[team_2_set2_sum] + ', ' if team2_set2_scores.has_key(team_2_set2_sum) else '') \
                + set2_games.game_type.name

        teams_scores = {
            'team1_set1_scores': team1_set1_scores,
            'team2_set1_scores': team2_set1_scores,
            'team1_set2_scores': team1_set2_scores,
            'team2_set2_scores': team2_set2_scores
        }

        teams_info = {
            'team1_set1_S1': games.filter(game_type__name__iexact='S1', set=1).first().team1_player1,
            'team2_set1_S1': games.filter(game_type__name__iexact='S1', set=1).first().team2_player1,
            'team1_set2_S1': games.filter(game_type__name__iexact='S1', set=2).first().team1_player1,
            'team2_set2_S1': games.filter(game_type__name__iexact='S1', set=2).first().team2_player1,

            'team1_set1_S2': games.filter(game_type__name__iexact='S2', set=1).first().team1_player1,
            'team2_set1_S2': games.filter(game_type__name__iexact='S2', set=1).first().team2_player1,
            'team1_set2_S2': games.filter(game_type__name__iexact='S2', set=2).first().team1_player1,
            'team2_set2_S2': games.filter(game_type__name__iexact='S2', set=2).first().team2_player1,

            'team1_set1_D1_player1': games.filter(game_type__name__iexact='D1', set=1).first().team1_player1,
            'team1_set1_D1_player2': games.filter(game_type__name__iexact='D1', set=1).first().team1_player2,
            'team2_set1_D1_player1': games.filter(game_type__name__iexact='D1', set=1).first().team2_player1,
            'team2_set1_D1_player2': games.filter(game_type__name__iexact='D1', set=1).first().team2_player2,

            'team1_set2_D1_player1': games.filter(game_type__name__iexact='D1', set=2).first().team1_player1,
            'team1_set2_D1_player2': games.filter(game_type__name__iexact='D1', set=2).first().team1_player2,
            'team2_set2_D1_player1': games.filter(game_type__name__iexact='D1', set=2).first().team2_player1,
            'team2_set2_D1_player2': games.filter(game_type__name__iexact='D1', set=2).first().team2_player2,

            'team1_set1_D2_player1': games.filter(game_type__name__iexact='D2', set=1).first().team1_player1,
            'team1_set1_D2_player2': games.filter(game_type__name__iexact='D2', set=1).first().team1_player2,
            'team2_set1_D2_player1': games.filter(game_type__name__iexact='D2', set=1).first().team2_player1,
            'team2_set1_D2_player2': games.filter(game_type__name__iexact='D2', set=1).first().team2_player2,

            'team1_set2_D2_player1': games.filter(game_type__name__iexact='D2', set=2).first().team1_player1,
            'team1_set2_D2_player2': games.filter(game_type__name__iexact='D2', set=2).first().team1_player2,
            'team2_set2_D2_player1': games.filter(game_type__name__iexact='D2', set=2).first().team2_player1,
            'team2_set2_D2_player2': games.filter(game_type__name__iexact='D2', set=2).first().team2_player2,
        }
    else:
        teams_info = {}
        teams_scores = {}

    template_name = 'race_match_detail' if is_race_match else 'match_detail'

    return render(request, 'match/%s.html' % template_name, {
        'selected_city': city_short_name if city_short_name else active_season.city.short_name,
        'match': match,
        'teams_info': teams_info,
        'teams_scores': teams_scores})
