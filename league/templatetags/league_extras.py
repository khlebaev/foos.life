import random
from django import template

from ..models import City, Season
import re

register = template.Library()


@register.inclusion_tag('menu/cities.html')
def cities_menu(selected_city):
    return {'cities': City.objects.all(), 'selected_city': selected_city}


@register.simple_tag
def selected_menu(request, pattern):
    selected_menu = 'menu-item-divided pure-menu-selected'

    if pattern and re.search(pattern + '$', request.path):
        return selected_menu
    elif request.path.strip('/') == 'league':
        active_season = Season.objects.filter(is_active=True)[0]
        first_city_short_name = active_season.city.short_name

        if pattern and first_city_short_name and re.search(first_city_short_name + '/$', pattern):
            return selected_menu

    return ''


@register.simple_tag
def race_match_games_by_set(match, set_number):
    return match.games_by_set(set_number)


@register.simple_tag
def random_int(a, b=None):
    if b is None:
        a, b = 0, a
    return random.randint(a, b)


@register.simple_tag
def teams_contains_small_image(teams):
    if any(x.image_small for x in teams):
        return True
    return False
