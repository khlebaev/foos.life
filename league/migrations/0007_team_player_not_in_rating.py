# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-09-25 00:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('league', '0006_match_forfeit'),
    ]

    operations = [
        migrations.AddField(
            model_name='teamplayer',
            name='is_not_in_rating',
            field=models.BooleanField(default=False),
        ),
    ]
